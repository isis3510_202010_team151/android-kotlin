package com.example.whealth.viewModel

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.whealth.model.LoginRepository
import com.example.whealth.model.LaunchBreakRepository
import com.example.whealth.model.LaunchSurveyRepository
import com.example.whealth.model.SignUPRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

class MyViewModel(
    private val loginRepository: LoginRepository,
    private val launchBreakRepository: LaunchBreakRepository,
    private val launchSurveyRepository: LaunchSurveyRepository
): ViewModel() {

    var loginResponse = ""

    fun login(username: String, token: String) {
        val job = viewModelScope.launch(Dispatchers.IO) {
            loginResponse = loginRepository.makeLoginRequest(username, token)
        }
        runBlocking {
            job.join()
        }
    }

    fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if(connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if(capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    return true
                }
                else if(capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    return true
                }
                else if(capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    return true
                }
            }
        }
        return false
    }

    fun launchActiveBreak(username: String) {
        val job = viewModelScope.launch(Dispatchers.IO) {
            launchBreakRepository.launchActivePause(username)
        }
    }

    fun launchHealthSurvey(username: String) {
        val job = viewModelScope.launch(Dispatchers.IO) {
            launchSurveyRepository.launchHealthSurvey(username)
        }
    }
}