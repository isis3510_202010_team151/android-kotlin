package com.example.whealth.model

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class LoginRepository {
    private val login = "https://w-health-backend.herokuapp.com/api/users/auth"
    // Function that makes the network request, blocking the current thread
    fun makeLoginRequest(username: String, token: String): String {
        val params = "$login/$username/$token"
        val url = URL(params)
        val conn = url.openConnection() as HttpURLConnection
        conn.requestMethod = "GET"

        var rta = ""
        BufferedReader(InputStreamReader(conn.inputStream)).use { br ->
            var line: String?
            while (br.readLine().also { line = it } != null) {
                rta = line!!
            }
        }
        return rta
    }
}