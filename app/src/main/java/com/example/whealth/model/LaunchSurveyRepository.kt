package com.example.whealth.model

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL

class LaunchSurveyRepository {
    private val healthSurvey = "https://w-health-backend.herokuapp.com/api/users/lastE_Survey"

    // Function that makes the network request, blocking the current thread
    fun launchHealthSurvey(username: String) {
        val params = "$healthSurvey/$username"
        val url = URL(params)
        val conn = url.openConnection() as HttpURLConnection
        conn.requestMethod = "POST"
        var rta = ""
        BufferedReader(InputStreamReader(conn.inputStream)).use { br ->
            var line: String?
            while (br.readLine().also { line = it } != null) {
                rta = line!!
            }
        }
    }
}