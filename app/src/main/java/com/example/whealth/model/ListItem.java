package com.example.whealth.model;

class ListItem {
    private String email;
    private  String image;

    public ListItem(String email,String imageUrl){
        this.email = email;
        this.image = imageUrl;
    }
    public String getEmail(){
        return email;
    }
    public String getImage(){
        return  image;
    }
}