package com.example.whealth.model

import android.util.Log
import com.google.gson.GsonBuilder
import com.google.gson.JsonParser
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class SignUPRepository {
    private val signup = "https://w-health-backend.herokuapp.com/api/users"
     fun makeSignUpRequest(
        name: String,
        email: String,
        token: String,
        corporation: String,
        isSupervisor: String
    ) {
        val userNew = JSONObject()
        userNew.put("name", name)
        userNew.put("email", email)
        userNew.put("password", token)
        userNew.put("coorporation", corporation)
        userNew.put("isSupervisor", isSupervisor)

        val jsonObjectString = userNew.toString()

         GlobalScope.launch(Dispatchers.IO) {
        val url = URL(signup)
        val conn = url.openConnection() as HttpURLConnection
        conn.requestMethod = "POST"
        conn.setRequestProperty("Content-Type", "application/json")
        conn.setRequestProperty("Accept", "application/json")
        conn.doInput = true
        conn.doOutput = true

        val outputStreamWriter = OutputStreamWriter(conn.outputStream)
        outputStreamWriter.write(jsonObjectString)
        outputStreamWriter.flush()

        // Check if the connection is successful
        val responseCode = conn.responseCode
        if (responseCode == HttpURLConnection.HTTP_OK) {
            val response = conn.inputStream.bufferedReader()
                .use { it.readText() }  // defaults to UTF-8
            withContext(Dispatchers.Main) {

                // Convert raw JSON to pretty JSON using GSON library
                val gson = GsonBuilder().setPrettyPrinting().create()
                val prettyJson = gson.toJson(JsonParser.parseString(response))
                Log.d("Pretty Printed JSON :", prettyJson)

            }
        } else {
            Log.e("HTTPURLCONNECTION_ERROR", responseCode.toString())
        }
         }
    }
}
