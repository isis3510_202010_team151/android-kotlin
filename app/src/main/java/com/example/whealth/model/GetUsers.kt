package com.example.whealth.model

import android.util.Log
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.google.gson.JsonObject
import com.google.gson.JsonParser
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject
import java.io.OutputStreamWriter
import java.net.HttpURLConnection
import java.net.URL

class GetUsers {
    private val signup = "https://w-health-backend.herokuapp.com/api/users"

    fun numSurveysDone():List<Int>{
        var numDone = 0
        var numFail = 0
        var total = 0
        val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
            throwable.printStackTrace()
        }
        runBlocking {
            val primerAsync = GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
                val url = URL(signup)
                val httpURLConnection = url.openConnection() as HttpURLConnection
                httpURLConnection.setRequestProperty(
                    "Accept",
                    "application/json"
                )
                httpURLConnection.requestMethod = "GET"
                httpURLConnection.doInput = true
                httpURLConnection.doOutput = false
                // Check if the connection is successful
                val responseCode = httpURLConnection.responseCode
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    val response = httpURLConnection.inputStream.bufferedReader()
                        .use { it.readText() }  // defaults to UTF-8

                    withContext(Dispatchers.Default) {

                        // Convert raw JSON to pretty JSON using GSON library
                        val gson = GsonBuilder().setPrettyPrinting().create()
                        val pretty = gson.toJson(JsonParser.parseString(response))
                        var obj = JSONObject(pretty)
                        var sessionArray: JSONArray = obj.optJSONArray("users")
                        var oni = JSONObject()
                        for (i in 0..(sessionArray.length() - 1)) {
                             oni = sessionArray.getJSONObject(i)
                            if(oni.has("isSupervisor")){
                                if(oni.getString("isSupervisor").equals("no")){
                                    total +=1
                                    if (oni.get("lastE_Survey").equals("")){
                                        numFail +=1
                                    }
                                    else{
                                        numDone +=1
                                    }
                                }
                            }
                        }
                    }
                } else {
                    Log.e("HTTPURLCONNECTION_ERROR", responseCode.toString())
                }
            }
            primerAsync.join()
        }
        val lista = listOf<Int>(numDone,numFail,total)
        return lista
    }
    fun getUsers(email:String):Boolean {
        var users = false
        runBlocking {
            val coroutineExceptionHandler = CoroutineExceptionHandler { _, throwable ->
                throwable.printStackTrace()
            }
            val job = GlobalScope.launch(Dispatchers.IO + coroutineExceptionHandler) {
                val url = URL(signup)
                val httpURLConnection = url.openConnection() as HttpURLConnection
                httpURLConnection.setRequestProperty(
                    "Accept",
                    "application/json"
                )
                httpURLConnection.requestMethod = "GET"
                httpURLConnection.doInput = true
                httpURLConnection.doOutput = false
                // Check if the connection is successful
                val responseCode = httpURLConnection.responseCode
                if (responseCode == HttpURLConnection.HTTP_OK) {
                    val response = httpURLConnection.inputStream.bufferedReader()
                        .use { it.readText() }  // defaults to UTF-8

                    withContext(Dispatchers.Default) {

                        // Convert raw JSON to pretty JSON using GSON library
                        val gson = GsonBuilder().setPrettyPrinting().create()
                        val pretty = gson.toJson(JsonParser.parseString(response))
                        var obj = JSONObject(pretty)
                        var sessionArray: JSONArray = obj.optJSONArray("users")
                        var emailOvjectI = ""
                        for (i in 0..(sessionArray.length() - 1)) {
                            val oni = sessionArray.getJSONObject(i)
                            if (oni.has("email")) {
                                emailOvjectI = oni.getString("email")
                                if (emailOvjectI.equals(email)) {
                                    users = true
                                    break
                                }
                            }
                        }

                    }
                } else {
                    Log.e("HTTPURLCONNECTION_ERROR", responseCode.toString())
                }

            }
            job.join()
        }
            return users
        }
}