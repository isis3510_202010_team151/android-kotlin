package com.example.whealth.view

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.whealth.R
import com.example.whealth.databinding.ActivityEmployeeHomeBinding
import com.example.whealth.model.LaunchBreakRepository
import com.example.whealth.model.LaunchSurveyRepository
import com.example.whealth.model.LoginRepository
import com.example.whealth.viewModel.MyViewModel
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.util.*
import java.util.concurrent.TimeUnit

class EmployeeHome : AppCompatActivity() {

    private lateinit var binding: ActivityEmployeeHomeBinding
    private lateinit var viewModel: MyViewModel
    private lateinit var loginRepository: LoginRepository
    private lateinit var launchBreakRepository: LaunchBreakRepository
    private lateinit var launchSurveyRepository: LaunchSurveyRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmployeeHomeBinding.inflate(layoutInflater)
        supportActionBar?.hide()
        setContentView(binding.root)

        //Getting UI elements
        loginRepository = LoginRepository()
        launchBreakRepository = LaunchBreakRepository()
        launchSurveyRepository = LaunchSurveyRepository()
        viewModel = MyViewModel(loginRepository, launchBreakRepository, launchSurveyRepository)
        binding.activeBreakButton.setOnClickListener {launchActiveBreak()}
        binding.customExerciseButton.setOnClickListener { launchCustomExercise() }
        binding.healthSurveyButton.setOnClickListener { launchHealthSurvey() }
        binding.logoutIcon.setOnClickListener { logout() }
        binding.profileIcon.setOnClickListener { profile() }
        createNotificationChannel()

        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        val savedEmail = sharedPref.getString("saved_email", "")
        val savedPass = sharedPref.getString("saved_pass", "")
        val futureTime = sharedPref.getString("saved_worktime", "")
        val lastDayOpened = sharedPref.getString("saved_day", "")
        val currentDate = LocalDateTime.now()
        if(futureTime.isNullOrEmpty() or lastDayOpened.isNullOrEmpty() or !lastDayOpened.equals(LocalDateTime.now().dayOfMonth.toString())) {
            editor.putString("saved_worktime", calculateFutureTime(currentDate))
            editor.putString("saved_day", LocalDateTime.now().dayOfMonth.toString())
            editor.apply()
            scheduleNotification()
        }
        else {
            binding.timer.text = futureTime
        }
        getCurrentDate(currentDate)
        binding.workerImage.setImageResource(R.drawable.worker)
        binding.logoutIcon.setImageResource(R.drawable.ic_baseline_login_24)
        binding.profileIcon.setImageResource(R.drawable.ic_baseline_account_circle_24)

        if(checkSurveyTime(savedEmail, savedPass)){
            val dialogBuilder = AlertDialog.Builder(this)
            val timer2 = object: CountDownTimer(1000, 1000) {
                override fun onTick(millisUntilFinished: Long) {
                }
                override fun onFinish() {
                    dialogBuilder.setMessage("You have not filled the health survey this week, we recommend you to fill it now")
                        .setCancelable(false)
                        .setPositiveButton("Fill the survey", DialogInterface.OnClickListener {
                                _, _ -> launchHealthSurvey()
                        })
                        .setNegativeButton("Later", DialogInterface.OnClickListener {
                                dialog, _ -> dialog.cancel()
                        })
                    val alert = dialogBuilder.create()
                    alert.setTitle("Fill your health survey")
                    alert.show()
                }
            }.start()
        }
    }

    private fun profile(){
        val intent = Intent(this, EmployeeProfile::class.java)
        startActivity(intent)
    }

    private fun scheduleNotification() {
        val intent = Intent(applicationContext, Notification::class.java)
        val title = "Take a break"
        val msg =  "You have been working for more than 3 hours, time to rest"
        intent.putExtra(titleExtra, title)
        intent.putExtra(messageExtra, msg)

        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext,
            notificationID,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val time = System.currentTimeMillis() + 10800000
        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            time,
            pendingIntent
        )
    }

    private fun createNotificationChannel() {
        val name = "Active break"
        val desc = "Active break needs to be launched"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelID, name, importance)
        channel.description = desc
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)
    }

    override fun onResume() {
        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        val currentDate = LocalDateTime.now()
        val futureTime = sharedPref.getString("saved_worktime", "")
        if(futureTime.isNullOrEmpty()) {
            val futureTime = calculateFutureTime(currentDate)
            editor.putString("saved_worktime", futureTime)
            editor.apply()
        }
        super.onResume()
    }

    override fun onBackPressed() {
        finishAffinity()
    }

    private fun logout(){
        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString("saved_email", "")
        editor.putString("saved_pass", "")
        editor.putString("saved_supervisor","")
        editor.apply()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }

    private fun getCurrentDate(currentDate: LocalDateTime) {
        val weekday = currentDate.getDayOfWeek().name.lowercase().replaceFirstChar { it.uppercase() }
        val day = currentDate.getDayOfMonth()
        val month = currentDate.getMonth().name.lowercase().replaceFirstChar { it.uppercase() }
        binding.weekdayText.text = "$weekday, $month ${day.toString()}"
    }

    private fun calculateFutureTime(currentDate: LocalDateTime): String {
        val nextTime = currentDate.plusHours(3)
        val hours = nextTime.hour
        val minutes = nextTime.minute
        binding.timer.text = "${hours.toString()}:${minutes.toString()}"
        return "${hours.toString()}:${minutes.toString()}"
    }

    private fun launchCustomExercise(){
        Toast.makeText(this, "Functionality not yet implemented", Toast.LENGTH_LONG).show()
    }

    private fun launchActiveBreak(){
        val intent = Intent(this, ActiveBreak::class.java)
        startActivity(intent)
    }

    private fun checkSurveyTime(savedEmail: String?, savedToken: String?): Boolean {
        if (viewModel.isOnline(this)) {
            if (savedEmail != null) {
                if (savedToken != null) {
                    viewModel.login(savedEmail, savedToken)
                }
            }
            val loginResponse = viewModel.loginResponse
            if (loginResponse.isNotEmpty()) {
                val user = JSONObject(loginResponse)
                val lastSurvey = user.getString("lastE_Survey")
                if(lastSurvey.length >= 11) {
                    var year = lastSurvey.subSequence(0, 4)
                    var month = lastSurvey.subSequence(5, 7)
                    var day = lastSurvey.subSequence(8, 10)
                    var dateStr = "$day/$month/$year"
                    val sdf = SimpleDateFormat("dd/MM/yyyy")
                    val lastSurveyDate: Date = sdf.parse(dateStr)
                    val date = LocalDateTime.now()
                    day = date.getDayOfMonth().toString()
                    month = date.monthValue.toString()
                    year = date.year.toString()
                    dateStr = "$day/$month/$year"
                    val currentDate: Date = sdf.parse(dateStr)
                    val diff: Long = currentDate.getTime() - lastSurveyDate.getTime()
                    val daysdiff = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS)
                    if (daysdiff < 7) {
                        return false
                    }
                }
            }
        }
        return true
    }

    private fun launchHealthSurvey(){
        val intent = Intent(this, HealthSurvey::class.java)
        startActivity(intent)
         }
}