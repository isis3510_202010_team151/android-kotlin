package com.example.whealth.view

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.whealth.R
import com.example.whealth.model.Data
import com.squareup.picasso.Picasso

class MyAdapter(private val dataList:List<Data>):RecyclerView.Adapter<MyHolder>() {


    private lateinit var context:Context
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyHolder {
       context=parent.context
        return MyHolder(LayoutInflater.from(context).inflate(R.layout.item_view,parent,false))
    }

    override fun onBindViewHolder(holder: MyHolder, position: Int) {

        val data = dataList[position]

        val emailText = holder.itemView.findViewById<TextView>(R.id.user_email)
        val picText = holder.itemView.findViewById<ImageView>(R.id.user_pic)

        emailText.text = data.email
        Picasso.get().load(data.profilePic).into(picText)
    }

    override fun getItemCount() = dataList.size
}