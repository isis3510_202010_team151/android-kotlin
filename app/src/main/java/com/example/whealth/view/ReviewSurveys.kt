package com.example.whealth.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.whealth.databinding.ActivityReviewSurveyBinding
import com.example.whealth.model.GetUsers
import com.example.whealth.model.LaunchBreakRepository
import com.example.whealth.model.LaunchSurveyRepository
import com.example.whealth.model.LoginRepository
import com.example.whealth.viewModel.MyViewModel
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.math.roundToInt


class ReviewSurveys : AppCompatActivity(){
    private lateinit var viewModel: MyViewModel
    private lateinit var loginRepository: LoginRepository
    private lateinit var launchBreakRepository: LaunchBreakRepository
    private lateinit var launchSurveyRepository: LaunchSurveyRepository
    private lateinit var getUsers: GetUsers
    private lateinit var binding:ActivityReviewSurveyBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityReviewSurveyBinding.inflate(layoutInflater)
        setContentView(binding.root)

        getUsers = GetUsers()
        loginRepository = LoginRepository()
        launchSurveyRepository = LaunchSurveyRepository()
        launchBreakRepository = LaunchBreakRepository()
        viewModel = MyViewModel(loginRepository, launchBreakRepository, launchSurveyRepository)

        binding.logoutIcon.setOnClickListener { logout() }
        if(viewModel.isOnline(this)){
            val totalInt = intent.getStringExtra("Total")
            val total = totalInt?.toInt()
            val doneInt = intent.getStringExtra("Done")
            val done = doneInt?.toInt()
            val notInt = intent.getStringExtra("Not")
            val not = notInt?.toInt()
            println(total)
            var result = (done?.times(100))?.toBigDecimal()
                ?.divide(total?.toBigDecimal() ,3,RoundingMode.HALF_UP)
            println(result)
            var result1 = (not?.times(100))?.toBigDecimal()
                ?.divide(total?.toBigDecimal(),3,RoundingMode.HALF_UP)
            println(result1)
            binding.textView2.text = (""+done)
            binding.textView4.text = (""+result+"%")
            binding.textView22.text = (""+not)
            binding.textView24.text = (""+ result1+"%")
            Toast.makeText(this, "All the information is updated", Toast.LENGTH_LONG).show()
        }
        else{
            val totalInt = intent.getStringExtra("Total")
            if(totalInt!=null){
                val total = totalInt?.toInt()
                val doneInt = intent.getStringExtra("Done")
                val done = doneInt?.toInt()
                val notInt = intent.getStringExtra("Not")
                val not = notInt?.toInt()
                println(total)
                var result = (done?.times(100))?.toBigDecimal()
                    ?.divide(total?.toBigDecimal() ,3,RoundingMode.HALF_UP)
                println(result)
                var result1 = (not?.times(100))?.toBigDecimal()
                    ?.divide(total?.toBigDecimal(),3,RoundingMode.HALF_UP)
                println(result1)
                binding.textView2.text = (""+total)
                binding.textView4.text = (""+result+"%")
                binding.textView22.text = (""+total)
                binding.textView24.text = (""+ result1+"%")

                Toast.makeText(this, "the information may have changes", Toast.LENGTH_LONG).show()
            }
            else {
                Toast.makeText(this, "Need internet to show information", Toast.LENGTH_LONG).show()
            }
        }
    }
    override fun onBackPressed() {
        val goSupervisor = Intent(this, SupervirsorHome::class.java)
        startActivity( goSupervisor)
    }
    private fun logout(){
        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString("saved_email", "")
        editor.putString("saved_pass", "")
        editor.putString("saved_supervisor","")
        editor.apply()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}