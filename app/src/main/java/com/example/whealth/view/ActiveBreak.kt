package com.example.whealth.view

import android.app.AlarmManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.widget.Toast
import com.example.whealth.R
import com.example.whealth.databinding.ActivityActiveBreakBinding
import com.example.whealth.model.LaunchBreakRepository
import com.example.whealth.model.LaunchSurveyRepository
import com.example.whealth.model.LoginRepository
import com.example.whealth.viewModel.MyViewModel

class ActiveBreak : AppCompatActivity() {

    private lateinit var binding: ActivityActiveBreakBinding
    private lateinit var viewModel: MyViewModel
    private lateinit var loginRepository: LoginRepository
    private lateinit var launchBreakRepository: LaunchBreakRepository
    private lateinit var launchSurveyRepository: LaunchSurveyRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityActiveBreakBinding.inflate(layoutInflater)
        supportActionBar?.hide()
        setContentView(binding.root)

        //Getting UI elements
        binding.launchBreakButton.setOnClickListener { launchActiveBreak() }
        binding.breakImage.setImageResource(R.drawable.activebreak)
        loginRepository = LoginRepository()
        launchSurveyRepository = LaunchSurveyRepository()
        launchBreakRepository = LaunchBreakRepository()
        viewModel = MyViewModel(loginRepository, launchBreakRepository, launchSurveyRepository)
        createNotificationChannel()
    }

    private fun createNotificationChannel() {
        val name = "Active break"
        val desc = "Active break is over"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val channel = NotificationChannel(channelID, name, importance)
        channel.description = desc
        val notificationManager = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(channel)

    }

    private fun launchActiveBreak(){

        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        val savedEmail = sharedPref.getString("saved_email", "")

        if (viewModel.isOnline(this)){
            if (savedEmail != null) {
                viewModel.launchActiveBreak(savedEmail)
                editor.putString("saved_worktime", "")
                editor.apply()
                scheduleNotification()
                binding.launchBreakButton.isEnabled = false
                binding.launchBreakButton.isClickable = false

                val break_timer = object: CountDownTimer(900000, 1000) {
                    override fun onTick(millisUntilFinished: Long) {
                        val totalSecs = millisUntilFinished / 1000
                        val mins = totalSecs / 60
                        val secs  = totalSecs % 60
                        binding.timerBreak.setText("$mins:$secs")
                    }

                    override fun onFinish() {
                        binding.timerBreak.setText("00:00")
                        binding.timerBreak.isEnabled = true
                        binding.timerBreak.isClickable = true
                    }
                }.start()
            }
        }
        else {
            Toast.makeText(this, "Mobile network not available, please check your connection", Toast.LENGTH_LONG).show()
        }
    }

    private fun scheduleNotification() {
        val intent = Intent(applicationContext, Notification::class.java)
        val title = "Break is over"
        val msg =  "15 minutes have already gone by, you should get back to work"
        intent.putExtra(titleExtra, title)
        intent.putExtra(messageExtra, msg)

        val pendingIntent = PendingIntent.getBroadcast(
            applicationContext,
            notificationID,
            intent,
            PendingIntent.FLAG_IMMUTABLE or PendingIntent.FLAG_UPDATE_CURRENT
        )

        val alarmManager = getSystemService(Context.ALARM_SERVICE) as AlarmManager
        val time = System.currentTimeMillis() + 900000
        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            time,
            pendingIntent
        )

    }
}