package com.example.whealth.view

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.whealth.databinding.ActivityMainBinding
import com.example.whealth.model.LaunchBreakRepository
import com.example.whealth.model.LaunchSurveyRepository
import com.example.whealth.model.LoginRepository
import com.example.whealth.viewModel.MyViewModel
import org.json.JSONObject

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private lateinit var viewModel: MyViewModel
    private lateinit var loginRepository: LoginRepository
    private lateinit var launchBreakRepository: LaunchBreakRepository
    private lateinit var launchSurveyRepository: LaunchSurveyRepository

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        supportActionBar?.hide()
        setContentView(binding.root)

        //Getting UI elements
        binding.signupButton.setOnClickListener { singUp() }
        binding.loginButton.setOnClickListener { login() }
        loginRepository = LoginRepository()
        launchBreakRepository = LaunchBreakRepository()
        launchSurveyRepository = LaunchSurveyRepository()
        viewModel = MyViewModel(loginRepository, launchBreakRepository, launchSurveyRepository)

        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val savedEmail = sharedPref.getString("saved_email", "")
        val savedPass = sharedPref.getString("saved_pass", "")
        val savedSup = sharedPref.getString("saved_supervisor", "")

        if(!savedEmail.isNullOrEmpty() and !savedPass.isNullOrEmpty()){
            if(savedSup.equals("yes")) {
                val intent = Intent(this, SupervirsorHome::class.java)
                startActivity(intent)
            }
            else {
                val intent = Intent(this, EmployeeHome::class.java)
                startActivity(intent)
            }

        }


    }

    private fun singUp(){
        val intent = Intent(this, SingUP::class.java)
        startActivity(intent)
    }

    private fun login() {
        val inputMail = binding.editTextTextEmailAddress.text
        val inputPass = binding.editTextTextPassword.text

        if(inputMail.isNullOrEmpty()) {
            Toast.makeText(this, "Mail cannot be empty", Toast.LENGTH_LONG).show()
        }
        else if(inputPass.isNullOrEmpty()) {
            Toast.makeText(this, "Password cannot be empty", Toast.LENGTH_LONG).show()
        }
        else {
            if (viewModel.isOnline(this)) {
                viewModel.login(inputMail.toString(), inputPass.toString())
                val loginResponse = viewModel.loginResponse
                if (loginResponse.isNotEmpty()) {
                    val user = JSONObject(loginResponse)
                    val userName = user.getString("name")
                    val corp = user.getString("coorporation")
                    val isSup = user.getString("isSupervisor")
                    val img = user.getString("profilePic")
                    val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
                    val editor = sharedPref.edit()
                    editor.putString("saved_email", inputMail.toString())
                    editor.putString("saved_pass", inputPass.toString())
                    editor.putString("saved_supervisor", isSup)
                    editor.putString("saved_name", userName)
                    editor.putString("saved_coorporation", corp)
                    editor.putString("saved_picture", img)
                    editor.apply()
                    binding.editTextTextEmailAddress.setText("")
                    binding.editTextTextPassword.setText("")
                    if(isSup.equals("yes")){
                        val intent = Intent(this, SupervirsorHome::class.java)
                        startActivity(intent)
                    }
                    else{
                        val intent = Intent(this, EmployeeHome::class.java)
                        startActivity(intent)
                    }

                } else {
                    Toast.makeText(this, "Mail or password are incorrect", Toast.LENGTH_LONG).show()
                }
            }
            else{
                Toast.makeText(this, "Mobile network not available, please check your connection", Toast.LENGTH_LONG).show()
            }
        }
    }
}