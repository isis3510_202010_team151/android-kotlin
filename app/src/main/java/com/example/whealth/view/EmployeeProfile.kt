package com.example.whealth.view

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.example.whealth.R
import com.example.whealth.databinding.ActivityEmployeeProfileBinding

class EmployeeProfile : AppCompatActivity() {

    private lateinit var binding: ActivityEmployeeProfileBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEmployeeProfileBinding.inflate(layoutInflater)
        supportActionBar?.hide()
        setContentView(binding.root)

        //Getting UI elements

        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val savedEmail = sharedPref.getString("saved_email", "")
        val savedName = sharedPref.getString("saved_name", "")
        val savedCompany = sharedPref.getString("saved_coorporation", "")
        val savedPicture = sharedPref.getString("saved_picture","")
        binding.employeeEmailText.text = savedEmail
        binding.employeeNameText.text =  savedName
        if(savedCompany.isNullOrEmpty() or savedCompany.equals("null")){
            binding.employeeCorpText.text = "Independent"
        }
        else {
            binding.employeeCorpText.text = savedCompany
        }

        Glide.with(this)
            .load(savedPicture)
            .placeholder(R.drawable.ic_baseline_account_circle_24)
            .into(binding.profilePicture)
    }
}