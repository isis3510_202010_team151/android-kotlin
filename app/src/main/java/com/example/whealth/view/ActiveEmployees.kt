package com.example.whealth.view

import android.app.ProgressDialog
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.example.whealth.R
import com.example.whealth.model.Data
import org.json.JSONObject

class ActiveEmployees : AppCompatActivity() {

    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: RecyclerView.Adapter<MyHolder>
    private  var URL_DATA = "https://w-health-backend.herokuapp.com/api/users"

    private lateinit var listItems : ArrayList<Data>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_active_employees)

        listItems = ArrayList()

        recyclerView = findViewById(R.id.recyclerView2)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = LinearLayoutManager(this)
        loadRecyclerViewData()
    }

    private fun loadRecyclerViewData() {
        val progressDialog = ProgressDialog(this)
        progressDialog.setMessage("Loading data ...")
        progressDialog.show()

        val stringRequest = StringRequest(Request.Method.GET, URL_DATA, {
            progressDialog.dismiss()
            var jsonObject = JSONObject(it)
            val  array = jsonObject.getJSONArray("users")
            var o = JSONObject()
            for (i in 0..(array.length() - 1)) {
                o = array.getJSONObject(i)
                if(o.getString("isSupervisor").equals("no")) {
                    var item = Data(
                        o.getString("name"),
                        o.getString("email"),
                        "https://t3.ftcdn.net/jpg/02/36/48/86/360_F_236488644_opXVvD367vGJTM2I7xTlsHB58DVbmtxR.jpg"
                    )
                    listItems.add(item)
                }
            }
            adapter = MyAdapter(listItems)
            recyclerView.adapter = adapter

        }, {

        })
        var requestQueue = Volley.newRequestQueue(this)
        requestQueue.add(stringRequest)

    }



}