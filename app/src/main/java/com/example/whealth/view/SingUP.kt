package com.example.whealth.view

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.CheckBox
import android.widget.TextView
import android.widget.Toast
import com.example.whealth.R
import com.example.whealth.databinding.ActivitySingUpBinding
import com.example.whealth.model.*
import com.example.whealth.viewModel.MyViewModel
import com.google.gson.JsonArray
import com.google.gson.JsonObject
import kotlinx.coroutines.*
import org.json.JSONArray
import org.json.JSONObject

class SingUP: AppCompatActivity() {
    private lateinit var viewModel: MyViewModel
    private lateinit var loginRepository: LoginRepository
    private lateinit var launchBreakRepository: LaunchBreakRepository
    private lateinit var getUsers:GetUsers
    private lateinit var signUPRepository: SignUPRepository
    private lateinit var launchSurveyRepository: LaunchSurveyRepository
    private lateinit var binding: ActivitySingUpBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySingUpBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //Getting UI elements
        val sharedPref = this.getSharedPreferences("userdetails", MODE_PRIVATE)
        val savedEmail = sharedPref.getString("saved_email", "")
        val savedPass = sharedPref.getString("saved_pass", "")
        val savedSup = sharedPref.getString("saved_supervisor", "")

        getUsers = GetUsers()
        loginRepository = LoginRepository()
        launchSurveyRepository = LaunchSurveyRepository()
        launchBreakRepository = LaunchBreakRepository()
        viewModel = MyViewModel(loginRepository, launchBreakRepository, launchSurveyRepository)
        signUPRepository = SignUPRepository()

        binding.button6.setOnClickListener{goLogin()}
        binding.button5.setOnClickListener{singUP()}

    }
    private fun goLogin(){
        binding.editTextTextPersonName.text.clear()
        binding.editTextTextEmailAddress4.text.clear()
        binding.editTextTextPassword4.text.clear()
        binding.corporation.text.clear()
        val tLogin = Intent(this, MainActivity::class.java)
        startActivity( tLogin)
    }
    private fun singUP(){
        val inputName = binding.editTextTextPersonName.text
        val inputEmail = binding.editTextTextEmailAddress4.text
        val inputPassword = binding.editTextTextPassword4.text
        val inputCorporation = binding.corporation.text

        if(inputName.isNullOrEmpty() || inputEmail.isNullOrEmpty() || inputPassword.isNullOrEmpty() || inputCorporation.isNullOrEmpty()){
            Toast.makeText(this, "You need to fill all fields", Toast.LENGTH_LONG).show()
        }
        else{

            var sup =""
            sup = if(binding.checkBox.isChecked()){
                "yes"
            }else{
                "no"
            }
            if (viewModel.isOnline(this)) {
                val register = getUsers.getUsers(inputEmail.toString())
                if(register == false) {
                    runBlocking {
                        val job = GlobalScope.launch(Dispatchers.IO ) {
                            signUPRepository.makeSignUpRequest(
                                inputName.toString(),
                                inputEmail.toString(),
                                inputPassword.toString(),
                                inputCorporation.toString(),
                                sup
                            )
                        }
                        job.join()
                    }
                    if (!binding.checkBox.isChecked()) {
                        Toast.makeText(this, "user successfully created", Toast.LENGTH_LONG).show()
                        val intent = Intent(this, MainActivity::class.java)
                        startActivity(intent)
                    } else {
                        val sharedPref = this.getSharedPreferences("userdetails", MODE_PRIVATE)
                        val editor = sharedPref.edit()
                        editor.putString("saved_email", inputEmail.toString())
                        editor.putString("saved_pass", inputPassword.toString())
                        editor.putString("saved_supervisor", sup)
                        editor.apply()
                        val intent = Intent(this, SupervirsorHome::class.java)
                        startActivity(intent)
                    }

                }
                else{
                    Toast.makeText(this, "the email is already registered", Toast.LENGTH_LONG).show()
                    binding.editTextTextPassword4.text.clear()
                    binding.editTextTextEmailAddress4.text.clear()
                }
            }
            else{
                Toast.makeText(this, "You need internet connection to register", Toast.LENGTH_LONG).show()
            }
        }

    }

}