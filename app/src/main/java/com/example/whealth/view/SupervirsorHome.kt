package com.example.whealth.view

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.whealth.databinding.ActivitySupervisorHomeBinding
import com.example.whealth.model.GetUsers
import com.example.whealth.model.LaunchBreakRepository
import com.example.whealth.model.LaunchSurveyRepository
import com.example.whealth.model.LoginRepository
import com.example.whealth.viewModel.MyViewModel
import java.io.File
import java.io.FileOutputStream
import java.io.ObjectOutputStream

class SupervirsorHome : AppCompatActivity() {

    private lateinit var binding:ActivitySupervisorHomeBinding
    private lateinit var viewModel: MyViewModel
    private lateinit var loginRepository: LoginRepository
    private lateinit var launchBreakRepository: LaunchBreakRepository
    private lateinit var launchSurveyRepository: LaunchSurveyRepository
    private lateinit var getUsers: GetUsers


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySupervisorHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)


        getUsers = GetUsers()
        loginRepository = LoginRepository()
        launchSurveyRepository = LaunchSurveyRepository()
        launchBreakRepository = LaunchBreakRepository()
        viewModel = MyViewModel(loginRepository, launchBreakRepository, launchSurveyRepository)

        binding.button8.setOnClickListener { totalEmployees() }
        binding.button9.setOnClickListener { activeEmployees() }
        binding.button10.setOnClickListener { offEmployees() }
        binding.button11.setOnClickListener { sickEmployees() }

        val sharedPref = this.getSharedPreferences("userdetails", MODE_PRIVATE)
        val savedTotal = sharedPref.getString("saved_Total", "")
        val savedDone = sharedPref.getString("saved_Done", "")
        val savedNot = sharedPref.getString("saved_Not", "")


        if(viewModel.isOnline(this)){
            var text = getUsers.numSurveysDone()
            val total = text[2]
            val done = text[0]
            val not = text[1]
            binding.textView9.text = (""+ total)
            binding.textView19.text = ("" + total)
            binding.textView20.text = (""+ 0)
            binding.textView21.text = (""+0)
            val sharedPref = this.getSharedPreferences("userdetails", MODE_PRIVATE)
            val editor = sharedPref.edit()
            editor.putString("saved_Total",(""+total))
            editor.putString("saved_Done", (""+done))
            editor.putString("saved_Not", (""+not))
            editor.apply()

        }
        else{
            if (savedTotal.equals("")){
                Toast.makeText(this, "Need internet", Toast.LENGTH_LONG).show()
            }
            else{
                binding.textView9.text = (""+ savedTotal)
                binding.textView19.text = ("" + savedTotal)
                binding.textView20.text = (""+ 0)
                binding.textView21.text = (""+0)
                Toast.makeText(this, "Data may be out of date", Toast.LENGTH_LONG).show()
            }

        }


        binding.button8.setOnClickListener { totalEmployees() }
        binding.button9.setOnClickListener { activeEmployees() }
        binding.button10.setOnClickListener { offEmployees() }
        binding.button11.setOnClickListener { sickEmployees() }
        binding.button7.setOnClickListener { goReview() }
        binding.logoutIcon.setOnClickListener { logout() }

    }

    private fun goReview(){
        val sharedPref = this.getSharedPreferences("userdetails", MODE_PRIVATE)
        val savedTotal = sharedPref.getString("saved_Total", "")
        val savedDone = sharedPref.getString("saved_Done", "")
        val savedNot = sharedPref.getString("saved_Not", "")

        val goReview = Intent(this, ReviewSurveys::class.java)
        goReview.putExtra("Total",(""+savedTotal) )
        goReview.putExtra("Done",(""+savedDone) )
        goReview.putExtra("Not",(""+savedNot) )
        startActivity( goReview)
    }

    private fun totalEmployees(){
        if(viewModel.isOnline(this)){
            val intent = Intent(this, TotalEmployees::class.java)
            startActivity(intent)
        }
        else{
            Toast.makeText(this, "You need internet to know your total employees", Toast.LENGTH_LONG).show()
        }

    }
    private fun activeEmployees(){
        if(viewModel.isOnline(this)){
            val intent = Intent(this, ActiveEmployees::class.java)
            startActivity(intent)

        }
        else{
            Toast.makeText(this, "You need internet to know your active employees", Toast.LENGTH_LONG).show()
        }
    }
    private fun offEmployees(){
        if(viewModel.isOnline(this)){
            val intent = Intent(this, OfflineEmployees::class.java)
            startActivity(intent)
        }
        else{
            Toast.makeText(this, "You need internet to know your offline employees", Toast.LENGTH_LONG).show()
        }
    }
    private fun sickEmployees(){
        if(viewModel.isOnline(this)){
            val intent = Intent(this, SickEmployees::class.java)
            startActivity(intent)
        }
        else{
            Toast.makeText(this, "You need internet to know your sick employees", Toast.LENGTH_LONG).show()
        }
    }
    override fun onBackPressed() {
        finishAffinity()
    }

    private fun logout(){
        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val editor = sharedPref.edit()
        editor.putString("saved_email", "")
        editor.putString("saved_pass", "")
        editor.putString("saved_supervisor","")
        editor.putString("saved_Total","")
        editor.putString("saved_Done", "")
        editor.putString("saved_Not", "")
        editor.apply()
        val intent = Intent(this, MainActivity::class.java)
        startActivity(intent)
    }
}