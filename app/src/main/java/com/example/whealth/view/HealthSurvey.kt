package com.example.whealth.view

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.whealth.R
import com.example.whealth.databinding.ActivityHealthSurveyBinding
import com.example.whealth.model.LaunchBreakRepository
import com.example.whealth.model.LaunchSurveyRepository
import com.example.whealth.model.LoginRepository
import com.example.whealth.viewModel.MyViewModel
import org.json.JSONObject


class HealthSurvey : AppCompatActivity() {

    private lateinit var binding: ActivityHealthSurveyBinding
    private lateinit var viewModel: MyViewModel
    private lateinit var loginRepository: LoginRepository
    private lateinit var launchSurveyRepository: LaunchSurveyRepository
    private lateinit var launchBreakRepository: LaunchBreakRepository


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityHealthSurveyBinding.inflate(layoutInflater)
        supportActionBar?.hide()
        setContentView(binding.root)

        //Getting UI elements
        binding.answerSurveyButton.setOnClickListener { launchHealthSurvey() }
        binding.healthImage.setImageResource(R.drawable.healthsurvey)
        loginRepository = LoginRepository()
        launchBreakRepository = LaunchBreakRepository()
        launchSurveyRepository = LaunchSurveyRepository()
        viewModel = MyViewModel(loginRepository, launchBreakRepository,launchSurveyRepository)

        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val savedEmail = sharedPref.getString("saved_email", "")
        val savedPass = sharedPref.getString("saved_pass", "")
        binding.lastHealthSurveyText.setText("Last health survey: "+getLastHealthSurvey(savedEmail, savedPass))
    }

    private fun launchHealthSurvey() {
        val sharedPref = this.getSharedPreferences("userdetails", Context.MODE_PRIVATE)
        val savedEmail = sharedPref.getString("saved_email", "")

        if (viewModel.isOnline(this)){
            if (savedEmail != null) {
                binding.answerSurveyButton.isEnabled = false
                binding.answerSurveyButton.isClickable = false
                viewModel.launchHealthSurvey(savedEmail)
            }
            val viewIntent = Intent(
                "android.intent.action.VIEW",
                Uri.parse("https://forms.gle/CJHwqpZyiYhjg5ub8")
            )
            startActivity(viewIntent)
        }
        else {
            Toast.makeText(this, "Mobile network not available, please check your connection", Toast.LENGTH_LONG).show()
        }
    }

    private fun getLastHealthSurvey(savedEmail: String?, savedToken: String?): String {
        if (viewModel.isOnline(this)) {
            if (savedEmail != null) {
                if (savedToken != null) {
                    viewModel.login(savedEmail, savedToken)
                }
            }
            val loginResponse = viewModel.loginResponse
            if (loginResponse.isNotEmpty()) {
                val user = JSONObject(loginResponse)
                val lastSurvey = user.getString("lastE_Survey")
                if(lastSurvey.length >= 11) {
                    val year = lastSurvey.subSequence(0, 4)
                    val month = lastSurvey.subSequence(5, 7)
                    val day = lastSurvey.subSequence(8, 10)
                    return "$day/$month/$year"
                }
            }
        }
        return "unable to retrieve last survey"
    }
}